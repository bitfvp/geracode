<?php
$page="Pagina principal";
//$css="style1";
include_once("../includes/head.php");
include_once("includes/topo.php");
?>
    <main class="container">
        <div class="row">
            <div class="col-md-12">
                <form action="index.php?aca=geracodigo" method="post">
                    <div class="mb-3">
                        <label for="tabela" class="form-label">TABELA</label>
                        <input required type="text" name="tabela" id="tabela" class="form-control" value="<?php
                        if (isset($_POST['tabela'])){
                            echo $_POST['tabela'];
                        }
                        ?>">
                    </div>
                    <div class="mb-3">
                        <label for="apelido" class="form-label">APELIDO DA TABELA</label>
                        <input required type="text" name="apelido" id="apelido" class="form-control" value="<?php
                        if (isset($_POST['apelido'])){
                            echo $_POST['apelido'];
                        }
                        ?>">
                    </div>
                    <div class="mb-3">
                        <label for="id" class="form-label">ID DA TABELA</label>
                        <input required type="text" name="id" id="id" class="form-control" value="<?php
                        if (isset($_POST['id'])){
                            echo $_POST['id'];
                        }
                        ?>">
                    </div>
                    <div class="mb-3">
                        <label for="campos" class="form-label">CAMPOS</label>
                        <textarea required name="campos" id="campos" class="form-control" cols="30" rows="10"><?php
                            if (isset($_POST['campos'])){
                                echo $_POST['campos'];
                            }
                            ?></textarea>
                    </div>

                    <div class="col-md-12 d-grid">
                        <input type="submit" value="ENVIAR" class="btn btn-success my-2" />
                    </div>
                </form>
            </div>
        </div>



        <div class="row">
            <div class="col-md-12">
                <h1 class="text-success">controllers/<?php if (isset($_POST['apelido'])){echo $_POST['apelido'];} ?>_load.php</h3>
                <br>
                <pree>
                    <?php
                    if (isset($_POST['campos'])){
                        echo $controller_load;
                    }
                    ?>
                </pree>
            </div>

            <div class="col-md-12">
                <h1 class="text-success">controllers/<?php if (isset($_POST['apelido'])){echo $_POST['apelido'];} ?>_insert.php</h3>
                    <br>
                    <pree>
                        <?php
                        if (isset($_POST['campos'])){
                            echo $controller_insert;
                        }
                        ?>
                    </pree>
            </div>

            <div class="col-md-12">
                <h1 class="text-success">controllers/<?php if (isset($_POST['apelido'])){echo $_POST['apelido'];} ?>_update.php</h3>
                    <br>
                    <pree>
                        <?php
                        if (isset($_POST['campos'])){
                            echo $controller_update;
                        }
                        ?>
                    </pree>
            </div>

            <div class="col-md-12">
                <h1 class="text-success">controllers/<?php if (isset($_POST['apelido'])){echo $_POST['apelido'];} ?>_delete.php</h3>
                    <br>
                    <pree>
                        <?php
                        if (isset($_POST['campos'])){
                            echo $controller_delete;
                        }
                        ?>
                    </pree>
            </div>


            <div class="col-md-12">
                <h1 class="text-success">models/<?php if (isset($_POST['apelido'])){echo ucwords($_POST['apelido']);} ?>.class.php</h3>
                    <br>
                    <pree>
                        <?php
                        if (isset($_POST['campos'])){
                            echo htmlentities('<?php')."<br>";
                            echo htmlentities('class '.ucwords($apelido).'{')."<br>";

                            echo $model_insert;
                            echo "<br>////////////////////////////////////////////////////<br>";
                            echo $model_update;
                            echo "<br>////////////////////////////////////////////////////<br>";
                            echo $model_delete;

                            echo htmlentities('')."<br>";
                            echo htmlentities('}//fim da classe')."<br>";

                        }
                        ?>
                    </pree>
            </div>

            <div class="col-md-12">
                    <pree>
                        <h1 class="text-success">header.php</h3>
                            <h3 class="text-info">include_once("models/<?php if (isset($_POST['apelido'])){echo ucwords($_POST['apelido']);} ?>.class.php");</h3>
                            <h3 class="text-info">include_once("controllers/<?php if (isset($_POST['apelido'])){echo $_POST['apelido'];} ?>_load.php");</h3>
                        <h3 class="text-info">include_once("controllers/<?php if (isset($_POST['apelido'])){echo $_POST['apelido'];} ?>_insert.php");</h3>
                        <h3 class="text-info">include_once("controllers/<?php if (isset($_POST['apelido'])){echo $_POST['apelido'];} ?>_update.php");</h3>
                        <h3 class="text-info">include_once("controllers/<?php if (isset($_POST['apelido'])){echo $_POST['apelido'];} ?>_delete.php");</h3>

                    </pree>
            </div>

            <div class="col-md-12">
                <h1 class="text-success">views/V<?php if (isset($_POST['apelido'])){echo ucwords($_POST['apelido']);} ?>_editar.php</h3>
                    <br>
                    <pree>
                        <?php
                        if (isset($_POST['campos'])){
                            echo $views_editar;

                        }
                        ?>
                    </pree>
            </div>

        </div>



    </main>

<?php include_once("../includes/footer.php"); ?>
</body>
</html>
